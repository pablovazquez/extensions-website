from django.contrib import admin
from django.template.defaultfilters import truncatechars

from .models import CannedResponse, ApprovalActivity


@admin.register(CannedResponse)
class CannedResponseAdmin(admin.ModelAdmin):
    def truncate_response(obj):
        return truncatechars(obj.response, 50)

    truncate_response.short_description = 'Response'

    list_display = ('name', truncate_response)
    list_filter = ('type',)


@admin.register(ApprovalActivity)
class ReviewActivityAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'date_created',
    )
