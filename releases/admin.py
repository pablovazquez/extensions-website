from django.contrib import admin

import releases.models

list_display_exclude = {'id'}


@admin.register(releases.models.Release)
class ReleaseAdmin(admin.ModelAdmin):
    list_display = ['__str__'] + [
        _.name for _ in releases.models.Release._meta.fields if _.name not in list_display_exclude
    ]
    list_filter = ('is_active', 'is_lts', 'date_released_on', 'date_supported_until')
    date_hierarchy = 'date_released_on'
