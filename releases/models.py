from django.db import models
import extensions.fields


class Release(models.Model):
    date_released_on = models.DateField(null=False, blank=False, verbose_name='Released on')
    date_supported_until = models.DateField(null=False, blank=False, verbose_name='Supported until')
    version = extensions.fields.VersionStringField(
        max_length=64, null=False, blank=False, unique=True
    )

    is_active = models.BooleanField(
        default=True,
        help_text=(
            'Is this release currently actively developed or supported?<br>'
            'Controls whether or not this release shows up in '
            '<code>blender_version_max</code> dropdown.'
        ),
    )
    is_lts = models.BooleanField(
        default=False, verbose_name='Is LTS', help_text='Is this a Long-term Support release?'
    )

    release_notes_url = models.URLField(null=False, blank=False, verbose_name='Release notes URL')

    class Meta:
        ordering = ('-is_active', '-date_released_on')

    def __str__(self) -> str:
        return f'Blender {self.version}{self.is_lts and " LTS" or ""}'

    @classmethod
    def as_choices(cls):
        """Return currently active releases as choices."""
        return ((release.version, str(release)) for release in cls.objects.filter(is_active=True))
