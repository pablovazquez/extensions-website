class Verb:
    """These constants are used to dispatch Action records,
    changing the values will result in a mismatch with historical values stored in db.
    """

    APPROVED = 'approved'
    COMMENTED = 'commented'
    RATED_EXTENSION = 'rated extension'
    REPORTED_EXTENSION = 'reported extension'
    REPORTED_RATING = 'reported rating'
    REQUESTED_CHANGES = 'requested changes'
    REQUESTED_REVIEW = 'requested review'
    UPLOADED_NEW_VERSION = 'uploaded new version'


class Flag:
    AUTHOR = 'author'
    MODERATOR = 'moderator'
    REVIEWER = 'reviewer'
