from django.utils.translation import gettext_lazy as _
from extended_choices import Choices

# Categories to group canned responses and make them easier to look through
CANNED_RESPONSE_CATEGORY_CHOICES = Choices(
    ('OTHER', 1, _('Other')),
    ('SECURITY', 2, _('Security')),
    ('PRIVACY', 3, _('Privacy')),
    ('DEVELOPMENT_PRACTICES', 4, _('Development Practices')),
)
