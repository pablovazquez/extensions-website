from factory.django import DjangoModelFactory
import factory
import factory.fuzzy
import secrets
import hashlib

from files.models import File


def generate_random_sha256():
    """Generate a random (but valid) sha256 hash."""
    random_string = secrets.token_hex(32)
    sha256_hash = hashlib.sha256(random_string.encode()).hexdigest()
    return f"sha256:{sha256_hash}"


class FileFactory(DjangoModelFactory):
    class Meta:
        model = File

    original_name = factory.LazyAttribute(lambda x: x.source)
    original_hash = factory.LazyFunction(lambda: generate_random_sha256())
    hash = factory.LazyAttribute(lambda x: x.original_hash)
    size_bytes = factory.Faker('random_int')
    source = factory.Faker('file_name', extension='zip')

    user = factory.SubFactory('common.tests.factories.users.UserFactory')
