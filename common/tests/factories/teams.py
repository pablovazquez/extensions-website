from factory.django import DjangoModelFactory

import teams.models


class TeamFactory(DjangoModelFactory):
    class Meta:
        model = teams.models.Team
