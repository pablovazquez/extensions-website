from datetime import timedelta

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone

from common.tests.factories.users import UserFactory
from common.tests.factories.extensions import create_approved_version
import users.tasks as tasks

User = get_user_model()


class TestTasks(TestCase):
    fixtures = ['dev', 'licenses']

    def test_handle_deletion_request_anonymized(self):
        now = timezone.now()
        # TODO: create some extensions/ratings/version linked to the test user
        # which means that this user also has some activity
        version = create_approved_version()
        user = version.extension.authors.first()
        user.date_deletion_requested = now - timedelta(days=30)
        user.save()

        with self.assertLogs('users.models', level='WARNING') as log:
            tasks.handle_deletion_request.task_function(pk=user.pk)
            self.assertRegex(
                log.output[0],
                f'Anonymized user pk={user.pk}',
            )

        # user wasn't deleted but anonymised
        user.refresh_from_db()
        self.assertFalse(user.is_active)
        self.assertEqual(user.full_name, '')
        self.assertTrue(user.email.startswith('del'))
        self.assertTrue(user.email.endswith('@example.com'))

        # TODO: check that publicly listed extensions/files/version remained as is

    def test_handle_deletion_request_deleted(self):
        now = timezone.now()
        user = UserFactory(
            email='mail1@example.com', date_deletion_requested=now - timedelta(days=30)
        )
        # TODO: create some extensions/ratings/version linked to the test user

        with self.assertLogs('users.models', level='WARNING') as log:
            tasks.handle_deletion_request.task_function(pk=user.pk)
            self.assertRegex(
                log.output[0],
                f'Deleted user pk={user.pk}',
            )

        # user got deleted
        with self.assertRaises(User.DoesNotExist):
            user.refresh_from_db()
