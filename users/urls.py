from django.urls import path, include

from users.views.webhooks import user_modified_webhook
import users.views.settings as settings

app_name = 'users'
urlpatterns = [
    path('webhooks/user-modified/', user_modified_webhook, name='webhook-user-modified'),
    path(
        'settings/',
        include(
            [
                path('profile/', settings.ProfileView.as_view(), name='my-profile'),
                path(
                    'profile/subscribe-notification-emails/',
                    settings.SubscribeNotificationEmailsView.as_view(),
                    name='subscribe-notification-emails',
                ),
                path('delete/', settings.DeleteView.as_view(), name='my-profile-delete'),
            ]
        ),
    ),
]
