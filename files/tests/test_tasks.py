from pathlib import Path
from unittest.mock import patch
import logging

from django.test import TestCase, override_settings

from common.tests.factories.files import FileFactory
from files.tasks import make_thumbnails
import files.models

TEST_MEDIA_DIR = Path(__file__).resolve().parent / 'media'


@override_settings(MEDIA_ROOT=TEST_MEDIA_DIR, REQUIRE_FILE_VALIDATION=True)
class TasksTest(TestCase):
    def test_make_thumbnails_fails_when_no_validation(self):
        file = FileFactory(original_hash='foobar', source='file/original_image_source.jpg')

        with self.assertRaises(files.models.File.validation.RelatedObjectDoesNotExist):
            make_thumbnails.task_function(file_id=file.pk)

    @patch('files.utils.make_thumbnails')
    def test_make_thumbnails_fails_when_validation_not_ok(self, mock_make_thumbnails):
        file = FileFactory(original_hash='foobar', source='file/original_image_source.jpg')
        files.models.FileValidation.objects.create(file=file, is_ok=False, results={})

        with self.assertLogs(level=logging.ERROR) as logs:
            make_thumbnails.task_function(file_id=file.pk)

        self.maxDiff = None
        self.assertEqual(
            logs.output[0], f"ERROR:files.tasks:File pk={file.pk} is flagged, won't make thumbnails"
        )

        mock_make_thumbnails.assert_not_called()

    @patch('files.utils.make_thumbnails')
    def test_make_thumbnails_fails_when_not_image_or_video(self, mock_make_thumbnails):
        file = FileFactory(
            original_hash='foobar', source='file/source.zip', type=files.models.File.TYPES.THEME
        )

        with self.assertLogs(level=logging.ERROR) as logs:
            make_thumbnails.task_function(file_id=file.pk)

        self.maxDiff = None
        self.assertEqual(
            logs.output[0],
            f'ERROR:files.tasks:File pk={file.pk} of type "Theme" is neither an image nor a video',
        )

        mock_make_thumbnails.assert_not_called()

    @patch('files.utils.resize_image')
    @patch('files.utils.Image')
    def test_make_thumbnails_for_image(self, mock_image, mock_resize_image):
        file = FileFactory(original_hash='foobar', source='file/original_image_source.jpg')
        files.models.FileValidation.objects.create(file=file, is_ok=True, results={})
        self.assertIsNone(file.thumbnail.name)
        self.assertEqual(file.metadata, {})

        make_thumbnails.task_function(file_id=file.pk)

        mock_image.open.assert_called_once_with(
            str(TEST_MEDIA_DIR / 'file' / 'original_image_source.jpg')
        )
        mock_image.open.return_value.close.assert_called_once()

        file.refresh_from_db()
        self.assertEqual(file.thumbnail.name, 'thumbnails/fo/foobar_1920x1080.png')
        self.assertEqual(
            file.metadata,
            {
                'thumbnails': {
                    '1080p': {'path': 'thumbnails/fo/foobar_1920x1080.png', 'size': [1920, 1080]},
                    '360p': {'path': 'thumbnails/fo/foobar_640x360.png', 'size': [640, 360]},
                },
            },
        )

    @patch('files.utils.resize_image')
    @patch('files.utils.Image')
    @patch('files.utils.FFmpeg')
    def test_make_thumbnails_for_video(self, mock_ffmpeg, mock_image, mock_resize_image):
        file = FileFactory(
            original_hash='deadbeef', source='file/path.mp4', type=files.models.File.TYPES.VIDEO
        )
        files.models.FileValidation.objects.create(file=file, is_ok=True, results={})
        self.assertIsNone(file.thumbnail.name)
        self.assertEqual(file.metadata, {})

        make_thumbnails.task_function(file_id=file.pk)

        mock_ffmpeg.assert_called_once_with()
        mock_image.open.assert_called_once_with(
            str(TEST_MEDIA_DIR / 'thumbnails' / 'de' / 'deadbeef.png')
        )
        mock_image.open.return_value.close.assert_called_once()

        file.refresh_from_db()
        # Check that the extracted frame is stored instead of the large thumbnail
        self.assertEqual(file.thumbnail.name, 'thumbnails/de/deadbeef.png')
        # Check that File metadata and thumbnail fields were updated
        self.assertEqual(
            file.metadata,
            {
                'thumbnails': {
                    '1080p': {'path': 'thumbnails/de/deadbeef_1920x1080.png', 'size': [1920, 1080]},
                    '360p': {'path': 'thumbnails/de/deadbeef_640x360.png', 'size': [640, 360]},
                },
            },
        )
