# Generated by Django 4.2.11 on 2024-04-12 09:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('files', '0004_alter_file_status'),
    ]

    operations = [
        migrations.RenameField(
            model_name='filevalidation',
            old_name='validation',
            new_name='results',
        ),
        migrations.AlterField(
            model_name='filevalidation',
            name='results',
            field=models.JSONField(),
        ),
        migrations.RemoveField(
            model_name='filevalidation',
            name='errors',
        ),
        migrations.RemoveField(
            model_name='filevalidation',
            name='notices',
        ),
        migrations.RemoveField(
            model_name='filevalidation',
            name='warnings',
        ),
        migrations.RenameField(
            model_name='filevalidation',
            old_name='is_valid',
            new_name='is_ok',
        ),
    ]
