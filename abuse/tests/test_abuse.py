from django.test import TestCase

from common.tests.factories.extensions import create_approved_version
from common.tests.factories.users import UserFactory

POST_DATA = {
    'message': 'test message',
    'reason': '127',
    'version': '',
}


class ReportTest(TestCase):
    def test_report_twice(self):
        version = create_approved_version()
        user = UserFactory()
        self.client.force_login(user)
        url = version.extension.get_report_url()
        _ = self.client.post(url, POST_DATA)
        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200)
