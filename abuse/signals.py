import logging

from actstream import action
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from abuse.models import AbuseReport
from constants.activity import Verb
from constants.base import (
    ABUSE_TYPE_EXTENSION,
    ABUSE_TYPE_RATING,
    ABUSE_TYPE_USER,
)

logger = logging.getLogger(__name__)


@receiver(post_save, sender=AbuseReport)
def _create_action_from_report(
    sender: object,
    instance: AbuseReport,
    created: bool,
    raw: bool,
    **kwargs: object,
) -> None:
    if not created:
        return
    if raw:
        return

    if instance.type == ABUSE_TYPE_EXTENSION:
        verb = Verb.REPORTED_EXTENSION
    elif instance.type == ABUSE_TYPE_RATING:
        verb = Verb.REPORTED_RATING
    elif instance.type == ABUSE_TYPE_USER:
        # TODO?
        return
    else:
        logger.warning(f'ignoring an unexpected AbuseReport type={instance.type}')
        return

    action.send(
        instance.reporter,
        verb=verb,
        target=instance.extension,
        action_object=instance,
    )


@receiver(pre_delete, sender=AbuseReport)
def _log_deletion(sender: object, instance: AbuseReport, **kwargs: object) -> None:
    instance.record_deletion()
