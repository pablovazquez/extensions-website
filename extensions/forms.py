import logging

from django import forms
from django.utils.translation import gettext_lazy as _
import django.core.exceptions

from files.validators import FileMIMETypeValidator
from constants.base import ALLOWED_PREVIEW_MIMETYPES

import extensions.models
import files.models
import reviewers.models

logger = logging.getLogger(__name__)


class EditPreviewForm(forms.ModelForm):
    class Meta:
        model = extensions.models.Extension.previews.through
        fields = (
            'caption',
            'position',
        )
        widgets = {
            'position': forms.HiddenInput(attrs={'data-position': ''}),
        }

    def __init__(self, *args, **kwargs):
        self.base_fields['caption'].widget.attrs.update({'placeholder': 'Describe the preview'})
        super().__init__(*args, **kwargs)


EditPreviewFormSet = forms.inlineformset_factory(
    extensions.models.Extension,
    extensions.models.Extension.previews.through,
    form=EditPreviewForm,
    extra=0,
)


class AddPreviewFileForm(forms.ModelForm):
    msg_unexpected_file_type = _('Choose a JPEG, PNG or WebP image, or an MP4 video')

    class Meta:
        model = files.models.File
        fields = ('caption', 'source', 'original_hash', 'hash')
        widgets = {'original_hash': forms.HiddenInput(), 'hash': forms.HiddenInput()}

    source = forms.FileField(
        allow_empty_file=False,
        required=True,
        validators=[
            FileMIMETypeValidator(
                allowed_mimetypes=ALLOWED_PREVIEW_MIMETYPES,
                message=msg_unexpected_file_type,
            ),
        ],
        widget=forms.ClearableFileInput(
            attrs={'accept': ','.join(ALLOWED_PREVIEW_MIMETYPES)},
        ),
    )
    caption = forms.CharField(max_length=255, required=False)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.extension = kwargs.pop('extension')
        self.base_fields['caption'].widget.attrs.update({'placeholder': 'Describe the preview'})
        super().__init__(*args, **kwargs)

    def clean_original_hash(self, *args, **kwargs):
        """Calculate original hash of the uploaded file."""
        if 'source' not in self.cleaned_data:
            return
        source = self.cleaned_data['source']
        return files.models.File.generate_hash(source)

    def clean_hash(self, *args, **kwargs):
        return self.cleaned_data['original_hash']

    def add_error(self, field, error):
        """Add hidden `original_hash`/`hash` errors to the visible `source` field instead."""
        if isinstance(error, django.core.exceptions.ValidationError):
            if getattr(error, 'error_dict', None):
                hash_error = error.error_dict.pop('hash', None)
                if hash_error:
                    error.error_dict['source'] = hash_error
                # `original_hash` is treated identically to `hash`, so its errors can be discarded
                error.error_dict.pop('original_hash', None)
        super().add_error(field, error)

    def save(self, *args, **kwargs):
        """Save Preview from the cleaned form data."""
        # Fill in missing fields from request and the source file
        self.instance.user = self.request.user

        instance = super().save(*args, **kwargs)

        # Create extension preview and save caption to it
        extensions.models.Preview.objects.create(
            file=instance,
            caption=self.cleaned_data['caption'],
            extension=self.extension,
        )
        return instance


class AddPreviewModelFormSet(forms.BaseModelFormSet):
    msg_duplicate_file = _('Please select another file instead of the duplicate')

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        self.extension = kwargs.pop('extension')
        super().__init__(*args, **kwargs)
        # Make sure formset doesn't attempt to select existing File records
        self.queryset = files.models.File.objects.none()

    def get_form_kwargs(self, *args, **kwargs):
        form_kwargs = super().get_form_kwargs(*args, **kwargs)
        form_kwargs['request'] = self.request
        form_kwargs['extension'] = self.extension
        return form_kwargs

    def get_unique_error_message(self, unique_check):
        """Replace duplicate `original_hash`/`hash` message with a more meaningful one."""
        if len(unique_check) == 1:
            field = unique_check[0]
            if field in ('original_hash', 'hash'):
                return self.msg_duplicate_file
        return super().get_unique_error_message(unique_check)


AddPreviewFormSet = forms.modelformset_factory(
    files.models.File,
    form=AddPreviewFileForm,
    formset=AddPreviewModelFormSet,
    extra=1,
)


class ExtensionUpdateForm(forms.ModelForm):
    # Messages for auto-generated activity
    msg_converted_to_draft = _('Converted to Draft')
    msg_awaiting_review = _('Ready for review')

    # Messages for additional validation
    msg_cannot_convert_to_draft = _(
        'An extension can be converted to draft only while it is Awating Review'
    )
    msg_need_previews = _('Please add at least one preview.')

    class Meta:
        model = extensions.models.Extension
        fields = (
            'description',
            'support',
        )

    def __init__(self, *args, **kwargs):
        """Pass the request and initialise all the nested form(set)s."""
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)
        if self.request.POST:
            edit_preview_formset = EditPreviewFormSet(
                self.request.POST, self.request.FILES, instance=self.instance
            )
            add_preview_formset = AddPreviewFormSet(
                self.request.POST,
                self.request.FILES,
                extension=self.instance,
                request=self.request,
            )
        else:
            edit_preview_formset = EditPreviewFormSet(instance=self.instance)
            add_preview_formset = AddPreviewFormSet(extension=self.instance, request=self.request)
        self.edit_preview_formset = edit_preview_formset
        self.add_preview_formset = add_preview_formset
        self.add_preview_formset.error_messages['too_few_forms'] = self.msg_need_previews

    def is_valid(self, *args, **kwargs) -> bool:
        """Validate all nested forms and form(set)s first."""
        # Require at least one preview image when requesting a review
        if 'submit_draft' in self.data:
            if not self.instance.previews.exists():
                self.add_preview_formset.min_num = 1
                self.add_preview_formset.validate_min = True

        is_valid_flags = [
            self.edit_preview_formset.is_valid(),
            self.add_preview_formset.is_valid(),
            super().is_valid(*args, **kwargs),
        ]
        return all(is_valid_flags)

    def clean(self):
        """Perform additional validation and status changes."""
        super().clean()
        # Convert to draft, if possible
        if 'convert_to_draft' in self.data:
            if self.instance.status != self.instance.STATUSES.AWAITING_REVIEW:
                self.add_error(None, self.msg_cannot_convert_to_draft)
            else:
                self.instance.status = self.instance.STATUSES.INCOMPLETE
                self.instance.converted_to_draft = True

        # Send the extension and version to the review, if possible
        if 'submit_draft' in self.data:
            self.instance.status = self.instance.STATUSES.AWAITING_REVIEW
            self.instance.sent_to_review = True
        return self.cleaned_data

    def save(self, *args, **kwargs):
        """Save the nested form(set)s, then the main form."""
        self.edit_preview_formset.save()
        self.add_preview_formset.save()
        if getattr(self.instance, 'converted_to_draft', False):
            reviewers.models.ApprovalActivity(
                user=self.request.user,
                extension=self.instance,
                type=reviewers.models.ApprovalActivity.ActivityType.AWAITING_CHANGES,
                message=self.msg_converted_to_draft,
            ).save()
        if getattr(self.instance, 'sent_to_review', False):
            reviewers.models.ApprovalActivity(
                user=self.request.user,
                extension=self.instance,
                type=reviewers.models.ApprovalActivity.ActivityType.AWAITING_REVIEW,
                message=self.msg_awaiting_review,
            ).save()
        return super().save(*args, **kwargs)


class ExtensionDeleteForm(forms.ModelForm):
    class Meta:
        model = extensions.models.Extension
        fields = []


class VersionForm(forms.ModelForm):
    class Meta:
        model = extensions.models.Version
        fields = {'file', 'release_notes'}

    def __init__(self, *args, **kwargs):
        """Limit 'file' choices to the initial file value."""
        super().__init__(*args, **kwargs)

        # Mark 'file' field as disabled so that Django form allows using its initial value.
        self.fields['file'].disabled = True

    def clean_file(self, *args, **kwargs):
        """Return file that was passed to the form via the initial values.

        This ensures that it doesn't have to be supplied by the form data.
        """
        return self.initial['file']


class VersionDeleteForm(forms.ModelForm):
    class Meta:
        model = extensions.models.Version
        fields = []
