# Generated by Django 4.0.6 on 2024-02-29 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('extensions', '0024_tagsaddons_tagsthemes_remove_version_tags_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tag',
            name='type',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Add-on'), (2, 'Theme')]),
        ),
    ]
