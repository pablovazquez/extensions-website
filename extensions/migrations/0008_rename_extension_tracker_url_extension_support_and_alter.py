# Generated by Django 4.0.6 on 2024-02-02 15:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('extensions', '0007_remove_extension_support'),
    ]

    operations = [
        migrations.RenameField(
            model_name='extension',
            old_name='tracker_url',
            new_name='support',
        ),
        migrations.AlterField(
            model_name='extension',
            name='support',
            field=models.URLField(blank=True, help_text='URL for reporting issues or contact details for support.', null=True),
        ),
    ]
