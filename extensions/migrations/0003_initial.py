# Generated by Django 4.0.6 on 2024-01-29 15:37

import common.fields
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import taggit.managers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('taggit', '0005_auto_20220424_2025'),
        ('files', '0002_initial'),
        ('extensions', '0002_initial'),
        ('teams', '0002_initial'),
        ('users', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='maintainer',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='extension',
            name='authors',
            field=common.fields.FilterableManyToManyField(related_name='extensions', through='extensions.Maintainer', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='extension',
            name='previews',
            field=common.fields.FilterableManyToManyField(related_name='extensions', through='extensions.Preview', to='files.file'),
        ),
        migrations.AddField(
            model_name='extension',
            name='tags',
            field=taggit.managers.TaggableManager(help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
        migrations.AddField(
            model_name='extension',
            name='team',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='teams.team'),
        ),
        migrations.AddField(
            model_name='versionreviewerflags',
            name='pending_rejection_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddConstraint(
            model_name='version',
            constraint=models.UniqueConstraint(fields=('extension', 'version'), name='version_extension_version_key'),
        ),
        migrations.AlterUniqueTogether(
            name='preview',
            unique_together={('extension', 'file')},
        ),
        migrations.AddConstraint(
            model_name='maintainer',
            constraint=models.UniqueConstraint(fields=('extension', 'user'), name='maintainer_extension_user'),
        ),
        migrations.AddConstraint(
            model_name='versionreviewerflags',
            constraint=models.CheckConstraint(check=models.Q(models.Q(('pending_rejection__isnull', True), ('pending_rejection_by__isnull', True)), ('pending_rejection__isnull', False), _connector='OR'), name='pending_rejection_all_none'),
        ),
    ]
