from django.apps import AppConfig


class RatingsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ratings'

    def ready(self):
        from actstream import registry
        import ratings.signals  # noqa: F401

        registry.register(self.get_model('Rating'))
