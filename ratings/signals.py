from actstream import action
from django.db.models.signals import pre_save, post_save, pre_delete
from django.dispatch import receiver

from constants.activity import Verb
from ratings.models import Rating


@receiver(pre_save, sender=Rating)
def _set_extension(sender, instance, *args, **kwargs):
    if not instance.extension_id:
        instance.extension_id = instance.version.extension_id


@receiver(post_save, sender=Rating)
def _update_rating_counters(sender, instance, *args, **kwargs):
    extension = instance.version.extension
    extension.recalculate_average_score()

    version = instance.version
    version.recalculate_average_score()


@receiver(post_save, sender=Rating)
def _create_action_from_rating(
    sender: object,
    instance: Rating,
    created: bool,
    raw: bool,
    **kwargs: object,
) -> None:
    if raw:
        return
    if not created:
        return

    action.send(
        instance.user,
        verb=Verb.RATED_EXTENSION,
        action_object=instance,
        target=instance.extension,
    )


@receiver(pre_delete, sender=Rating)
def _log_deletion(sender: object, instance: Rating, **kwargs: object) -> None:
    instance.record_deletion()
